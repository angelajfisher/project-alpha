from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from projects.models import Project
from tasks.models import Task


# Create your views here.
def user_login(request):
    errored = False  # Whether to send error message to user
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                errored = True  # Login failed, so display error message
    else:
        form = LoginForm()
    context = {"form": form,
               "errored": errored}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )

                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "The passwords do not match")
    else:
        form = SignupForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)


def homepage(request):
    if request.user.is_authenticated:
        user = request.user
        is_authenticated = True
        projects = Project.objects.filter(owner=request.user),
        tasks = Task.objects.filter(assignee=request.user),
    else:
        user = None
        is_authenticated = False
        projects = None
        tasks = None
    context = {
        "user": user,
        "is_authenticated": is_authenticated,
        "projects": projects,
        "tasks": tasks,
    }
    return render(request, "accounts/homepage.html", context)
