from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    context = {"projects": Project.objects.filter(owner=request.user)}
    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, id):
    context = {"project": get_object_or_404(Project, id=id)}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/project_create.html", context)


@login_required
def edit_project(request, id):
    project = get_object_or_404(Project, id=id)

    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            form.save()
            return redirect("show_project", id)
    else:
        form = ProjectForm(instance=project)
    context = {
        "project": project,
        "form": form,
    }
    return render(request, "projects/project_edit.html", context)
