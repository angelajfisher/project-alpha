from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
def create_task(request):
    user = request.user
    if request.method == "POST":
        form = TaskForm(user, request.POST)
        if form.is_valid():
            form = form.save(False)
            form.assignee = user
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(user)
    context = {"form": form}
    return render(request, "tasks/task_create.html", context)


@login_required
def show_my_tasks(request):
    context = {"tasks": Task.objects.filter(assignee=request.user)}
    return render(request, "tasks/task_list.html", context)


@login_required
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    user = request.user

    if request.method == "POST":
        form = TaskForm(user, request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskForm(user, instance=task)
    context = {
        "task": task,
        "form": form,
    }
    return render(request, "tasks/task_edit.html", context)
